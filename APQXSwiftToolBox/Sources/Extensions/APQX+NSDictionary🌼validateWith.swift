//
//  APQX+NSDictionary.swift
//  APQXSwiftToolBox
//
//  Created by Bonifatio Hartono on 5/11/16.
//  Copyright © 2016 Bonifatio Hartono. All rights reserved.
//

import Foundation
extension NSDictionary{
    enum eValidation: ErrorType{
        case ErrTypeMismatch(key: String, found: String)
        case ErrMissingKey(key: String)
        case ErrBaseReference(fileReferenceLiteral: String)
    }
    
    public func 🌼validateWithRef(fileReferenceLiteral: String) throws {
        let anURL = NSURL(fileReferenceLiteral: fileReferenceLiteral)
        guard let aBaseDict:NSDictionary! = NSDictionary(contentsOfURL: anURL) else {
            throw eValidation.ErrBaseReference(fileReferenceLiteral: fileReferenceLiteral)
        }
        try self.🌼validateWith(aBaseDict!)
    }
    
    public func 🌼validateWith(base: NSDictionary) throws {
        
        /* Dictionary to array conversion */
        do {
            try self.letsTraverse(base, payloadArray: [self], currentPath: "akar.")
        }
        catch { throw error }
    }
    
    func letsTraverse(base: NSDictionary, payloadArray: NSArray, currentPath: String) throws {
        
        /* Initialize iDX for use on the path */
        var iDX = -1
        
        /* Iterates the payload array
         TODO: review about using enumerator here.
         */
        for payload in payloadArray{
            
            /* Increment idx */
            iDX += 1
            
            /* Iterates all key in base dictionary */
            for baseKey in base.allKeys{
                
                /* Making sure the key exists in payload */
                guard (payload.objectForKey(baseKey) != nil) else{
                    throw eValidation.ErrMissingKey(key: baseKey as! String)
                }
                
                /* About Testing type match for Bool,
                 We don't test against Bool because Int and Bool type are overlap.
                 Bool type will be tested against Int instead.
                 */
                
                /* Testing type match for Number */
                if payload.objectForKey(baseKey) is NSNumber{
                    guard base.objectForKey(baseKey) is NSNumber else{
                        throw eValidation.ErrTypeMismatch(key: baseKey as! String, found: "NSNumber")
                    }
                    continue
                }
                
                /* Testing type match for String */
                if payload.objectForKey(baseKey) is NSString{
                    guard base.objectForKey(baseKey) is String else{
                        throw eValidation.ErrTypeMismatch(key: baseKey as! String, found: "NSString")
                    }
                    continue
                }
                
                /* Testing type match for NSDate */
                if payload.objectForKey(baseKey) is NSDate{
                    guard base.objectForKey(baseKey) is NSDate else{
                        throw eValidation.ErrTypeMismatch(key: baseKey as! String, found: "NSDate")
                    }
                    continue
                }
                
                /* Testing type match for NSDictionary */
                if payload.objectForKey(baseKey) is NSDictionary{
                    guard base.objectForKey(baseKey) is NSDictionary else{
                        throw eValidation.ErrTypeMismatch(key: baseKey as! String, found: "NSDictionary")
                    }
                    
                    /* Getting the count of dictionary and skip if the count is 0 */
                    let payloadDict:NSDictionary = payload.objectForKey(baseKey) as! NSDictionary
                    let baseDict:NSDictionary = base.objectForKey(baseKey) as! NSDictionary
                    guard payloadDict.count > 0 else { continue }
                    guard baseDict.count > 0 else { continue }
                    
                    do {
                        try self.letsTraverse(baseDict,
                                              payloadArray: [payloadDict],
                                              currentPath: "\(currentPath)\(iDX).\(baseKey)")
                    }
                    catch { throw error }
                    continue
                }
                
                /* Testing type match for NSArray */
                if payload.objectForKey(baseKey) is NSArray{
                    guard base.objectForKey(baseKey) is NSArray else{
                        throw eValidation.ErrTypeMismatch(key: baseKey as! String, found: "NSArray")
                    }
                    
                    /* Getting the count of array and skip if the count is 0 */
                    let payloadArr:NSArray = payload.objectForKey(baseKey) as! NSArray
                    let baseArray:NSArray = base.objectForKey(baseKey) as! NSArray
                    guard baseArray.count > 0 else { continue }
                    guard payloadArray.count > 0 else{ continue }
                    
                    /* Guard to allow only base array and payload array that has a dictionary content to flow through */
                    guard baseArray.objectAtIndex(0) is NSDictionary &&
                        payloadArray.objectAtIndex(0) is NSDictionary else{
                            continue
                    }
                    do {
                        try self.letsTraverse(baseArray.objectAtIndex(0) as! NSDictionary,
                                              payloadArray: payloadArr,
                                              currentPath: "\(currentPath)\(iDX).\(baseKey)")
                    }
                    catch { throw error }
                    continue
                }
            }
        }
    }
}
