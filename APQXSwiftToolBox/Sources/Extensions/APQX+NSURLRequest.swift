//
//  APQX+NSURLRequest.swift
//  APQXSwiftToolBox
//
//  Created by Bonifatio Hartono on 5/5/16.
//  Copyright © 2016 Bonifatio Hartono. All rights reserved.
//

import Foundation

extension NSURLRequest {
    func 🌼dataTask( closure: (NSData?, NSURLResponse?, NSError?) -> Void) {
        let aSession : NSURLSession = NSURLSession.sharedSession()
        let aDataTask : NSURLSessionDataTask = aSession.dataTaskWithRequest(self) {
            (inData, inResponse, inError) in closure(inData, inResponse, inError)
        }
        aDataTask.resume()
    }
}
